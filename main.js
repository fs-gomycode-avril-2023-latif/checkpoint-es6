/**
 * Implémentez la fonction unique_in_order qui prend comme argument une séquence et retourne une liste d’éléments sans aucun élément ayant la même valeur les uns à côté des autres et en préservant l’ordre d’origine des éléments.

Par exemple:

uniqueInOrder('AAAABBBCCDAABBB') == ['A', 'B', 'C', 'D', 'A', 'B']
uniqueInOrder('ABBCcAD')         == ['A', 'B', 'C', 'c', 'A', 'D']
uniqueInOrder([1,2,2,3,3])       == [1,2,3]
 */

function uniqueInOrder(sequence) {
  var uniqueList = [];
  var lastElement = null;

  for (var i = 0; i < sequence.length; i++) {
    var element = sequence[i];
    if (element !== lastElement) {
      uniqueList.push(element);
      lastElement = element;
    }
  }

  return uniqueList;
}

// var uniqueInOrder=function(iterable){
//     return [...iterable].filter((a, i) => a !== iterable[i-1])
// }

function getCount(chaine) {
  const voyelles = "aeiouy";
  let nombreVoyelles = 0;
  let voyelleSespace = [];

  for (let i = 0; i < chaine.length; i++) {
    const caractere = chaine[i].toLowerCase();
    for (const elt of caractere) {
      if (elt == " ") {
        elt.replace(" ", "a");
      }
      voyelleSespace.push(elt);
    }
    if (voyelles.includes(caractere)) {
      nombreVoyelles++;
    }
  }

  return nombreVoyelles;
}
console.log(getCount("my pyx"));

/**
 * Votre tâche consiste à créer une fonction qui effectue quatre opérations mathématiques de base.

La fonction doit prendre trois arguments - operation(string/char), value1(number), value2(number).
La fonction doit renvoyer le résultat des nombres après avoir appliqué l’opération choisie.

Exemples(Opérateur, valeur1, valeur2) --> sortie
('+', 4, 7) --> 11
('-', 15, 18) --> -3
('*', 5, 5) --> 25
('/', 49, 7) --> 7
 */

function basicOp(operation, valeur1, valeur2) {
  let resultat;

  switch (operation) {
    case "+":
      resultat = valeur1 + valeur2;
      break;
    case "-":
      resultat = valeur1 - valeur2;
      break;
    case "*":
      resultat = valeur1 * valeur2;
      break;
    case "/":
      resultat = valeur1 / valeur2;
      break;
    default:
      console.log("Opération non valide");
      return;
  }

  return resultat;
}

/**
 * Implémentez une fonction qui accepte 3 valeurs entières a, b, c. La fonction doit retourner true si un triangle peut être construit avec les côtés d’une longueur donnée et false dans n’importe quel autre cas.

(Dans ce cas, tous les triangles doivent avoir une surface supérieure à 0 pour être acceptés).
 */

function isTriangle(a, b, c) {
  const condition1 = a + b > c;
  const condition2 = b + c > a;
  const condition3 = c + a > b;

  return condition1 && condition2 && condition3;
}

/**
 * Vous connaissez peut-être de grands carrés parfaits. Mais qu’en est-il du PROCHAIN ?

Terminez la méthode qui trouve le carré parfait intégral suivant après celui passé en paramètre. Rappelons qu’un carré parfait intégral est un entier n tel que sqrt(n) est aussi un entier. findNextSquare

Si le paramètre n’est pas lui-même un carré parfait, alors doit être retourné. Vous pouvez supposer que le paramètre n’est pas négatif.-1

Exemples:(Entrée --> sortie)

121 --> 144
625 --> 676
114 --> -1 since 114 is not a perfect square
 */

function findNextSquare(n) {
  // Vérifier si le nombre n est un carré parfait
  const sqrt = Math.sqrt(n);
  if (Number.isInteger(sqrt)) {
    // Trouver le prochain carré parfait
    const nextSquare = Math.pow(sqrt + 1, 2);
    return nextSquare;
  } else {
    return -1;
  }
}
